﻿using WooliesX.Model.Checkout;
using WooliesX.Service;
using Xunit;

namespace WooliesX.Unit
{
    public class TrolleyTotalTest
    {
        [Fact]
        public void ItShouldCalculateCorrectTotalWhenDiscountIsNotOrderCorrectly()
        {
            var trolley = new Trolley
            {
                Products = new[]
                {
                    new Product
                    {
                        Name = "1",
                        Price = 2.0M
                    },
                    new Product
                    {
                        Name = "2",
                        Price = 5.0M
                    }
                },
                Specials = new[]
                {
                    new Special
                    {
                        Quantities = new[]
                        {
                            new Quantity
                            {
                                Name = "1",
                                QuantityValue = 3F
                            },
                            new Quantity
                            {
                                Name = "2",
                                QuantityValue = 0F
                            }
                        },
                        Total = 5.0M
                    },
                    new Special
                    {
                        Quantities = new[]
                        {
                            new Quantity
                            {
                                Name = "1",
                                QuantityValue = 1F
                            },
                            new Quantity
                            {
                                Name = "2",
                                QuantityValue = 2F
                            }
                        },
                        Total = 10.0M
                    }
                },
                Quantities = new[]
                {
                    new Quantity
                    {
                        Name = "1",
                        QuantityValue = 3F
                    },
                    new Quantity
                    {
                        Name = "2",
                        QuantityValue = 2F
                    }
                }
            };

            var trolleyTotalService = new TrolleyService();
            var total = trolleyTotalService.CalculateMinimumTotal(trolley);

            Assert.Equal(14, total);
        }

        [Fact]
        public void ItShouldCalculateCorrectWhenNoDiscountApply()
        {
            var trolley = new Trolley
            {
                Products = new[]
                {
                    new Product
                    {
                        Name = "A",
                        Price = 99M
                    },
                    new Product
                    {
                        Name = "B",
                        Price = 88M
                    }
                },
                Specials = new[]
                {
                    new Special
                    {
                        Quantities = new[]
                        {
                            new Quantity
                            {
                                Name = "A",
                                QuantityValue = 10F
                            },
                            new Quantity
                            {
                                Name = "B",
                                QuantityValue = 5F
                            }
                        },
                        Total = 44M
                    }
                },
                Quantities = new[]
                {
                    new Quantity
                    {
                        Name = "A",
                        QuantityValue = 3F
                    },
                    new Quantity
                    {
                        Name = "B",
                        QuantityValue = 2F
                    }
                }
            };

            var trolleyTotalService = new TrolleyService();
            var total = trolleyTotalService.CalculateMinimumTotal(trolley);

            Assert.Equal(473, total);
        }
    }
}