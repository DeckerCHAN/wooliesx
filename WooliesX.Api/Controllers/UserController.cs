using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WooliesX.Api.Controllers
{
    [Route("api/answers/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration Configuration;

        public UserController(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        [HttpGet]
        public IDictionary<string, string> Get()
        {
            return new Dictionary<string, string>
            {
                {"name", this.Configuration["TestUser:Name"]},
                {"token", this.Configuration["TestUser:Token"]}
            };
        }
    }
}