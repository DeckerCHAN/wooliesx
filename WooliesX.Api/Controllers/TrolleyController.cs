using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WooliesX.Model.Checkout;
using WooliesX.Service;

namespace WooliesX.Api.Controllers
{
    [Route("api/answers/trolleyTotal")]
    [ApiController]
    public class TrolleyController : ControllerBase
    {
        private readonly ILogger<TrolleyController> Logger;
        private readonly ITrolleyService TrolleyService;

        public TrolleyController(ILogger<TrolleyController> logger, ITrolleyService trolleyService)
        {
            this.Logger = logger;
            this.TrolleyService = trolleyService;
        }

        [HttpPost]
        public async Task<decimal> Post(Trolley trolley)
        {
            return await Task.FromResult(this.TrolleyService.CalculateMinimumTotal(trolley));
        }
    }
}