using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WooliesX.Products.Sorting;
using WooliesX.Service;

namespace WooliesX.Api.Controllers
{
    [Route("api/answers/[controller]")]
    [ApiController]
    public class SortController : ControllerBase
    {
        private readonly IProductService ProductService;
        private readonly SortStrategyFactory SortStrategyFactory;

        public SortController(SortStrategyFactory sortStrategyFactory, IProductService productService)
        {
            this.SortStrategyFactory = sortStrategyFactory;
            this.ProductService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string sortOption)
        {
            var strategy = await this.SortStrategyFactory.BuildStrategy(sortOption);
            if (strategy == null) return this.BadRequest(new {message = "bad request"});

            return this.Ok(await this.ProductService.FetchSortedProductListByStrategy(strategy));
        }
    }
}