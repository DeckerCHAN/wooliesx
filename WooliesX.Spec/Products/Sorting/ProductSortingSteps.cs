using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Moq;
using Newtonsoft.Json;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using WooliesX.Model.Shopping;
using WooliesX.Products.Sorting;
using WooliesX.Service;
using Xunit;

namespace WooliesX.Spec.Products.Sorting
{
    [Binding]
    public class ProductSortingSteps : StepBase
    {
        public IList<Product> SortedProduct;
        public IProductSortStrategy Strategy;

        [Given(@"Configuration ""(.*)"" as ""(.*)""")]
        public void GiveConfiguration(string key, string value)
        {
            this.MockConfiguration.Setup(x => x[key]).Returns(value);
        }

        [Given(@"Mocked products to ""(.*)"":")]
        public void GivenMockedProducts(string url, Table table)
        {
            var products = table.CreateSet<Product>();

            this.MockHttpClient.Setup(x => x.GetAsyncWithRetry<IList<Product>>(It.Is<string>(y => y.StartsWith(url))))
                .ReturnsAsync(products.ToList());
        }


        [Given(@"Mocked history to ""(.*)"":")]
        public void GivenMockedHistoryTo(string url, Table table)
        {
            var history = table.Rows.Select(x => new ShopperHistory
            {
                CustomerId = Convert.ToInt32(x["CustomerId"]),
                Products = new[]
                {
                    new Product
                    {
                        Name = x["ProductName"],
                        Price = Convert.ToDecimal(x["ProductPrice"]),
                        Quantity = Convert.ToSingle(x["ProductQuantity"])
                    }
                }
            });

            this.MockHttpClient.Setup(x =>
                    x.GetAsyncWithRetry<IList<ShopperHistory>>(It.Is<string>(y => y.StartsWith(url))))
                .ReturnsAsync(history.ToList());
        }


        [When(@"I give ""(.*)"" sorting")]
        public async Task WhenIGiveSorting(string sortingName)
        {
            using (var scope = this.Container.BeginLifetimeScope())
            {
                var factory = scope.Resolve<SortStrategyFactory>();
                this.Strategy = await factory.BuildStrategy(sortingName);
            }
        }

        [When(@"Use it to sort")]
        public async Task WhenUseItToSort()
        {
            using (var scope = this.Container.BeginLifetimeScope())
            {
                var productService = scope.Resolve<IProductService>();
                this.SortedProduct = await productService.FetchSortedProductListByStrategy(this.Strategy);
            }
        }

        [Then(@"The products should looks like this:")]
        public void ThenProductsShouldLooksLikeThis(Table table)
        {
            var expectedProducts = table.CreateSet<Product>();
            Assert.Equal(JsonConvert.SerializeObject(this.SortedProduct),
                JsonConvert.SerializeObject(expectedProducts));
        }
    }
}