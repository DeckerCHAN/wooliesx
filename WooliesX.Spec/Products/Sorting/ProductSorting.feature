Feature: Product Sorting
  In order to make sure the the sorting operation works as designed
  I want to try those sorting method one by one

  Scenario: Sort by Price Low to High
    Given Configuration "BaseUrl" as "http://www.example.com"
    And Configuration "ApiUrls:Products" as "/api/resource/products"
    And Mocked products to "http://www.example.com/api/resource/products":
      | Name | Price | Quantity |
      | A    | 99    | 0.0      |
      | B    | 88    | 0.0      |
      | C    | 77    | 0.0      |
    When I give "LOW" sorting
    And Use it to sort
    Then The products should looks like this:
      | Name | Price | Quantity |
      | C    | 77    | 0.0      |
      | B    | 88    | 0.0      |
      | A    | 99    | 0.0      |


  Scenario: Sort by Price High to Low
    Given Configuration "BaseUrl" as "http://www.example.com"
    And Configuration "ApiUrls:Products" as "/api/resource/products"
    And Mocked products to "http://www.example.com/api/resource/products":
      | Name | Price | Quantity |
      | B    | 88    | 0.0      |
      | C    | 77    | 0.0      |
      | A    | 99    | 0.0      |

    When I give "HIGH" sorting
    And Use it to sort
    Then The products should looks like this:
      | Name | Price | Quantity |
      | A    | 99    | 0.0      |
      | B    | 88    | 0.0      |
      | C    | 77    | 0.0      |


  Scenario: Sort by Name Ascending
    Given Configuration "BaseUrl" as "http://www.example.com"
    And Configuration "ApiUrls:Products" as "/api/resource/products"
    And Mocked products to "http://www.example.com/api/resource/products":
      | Name | Price | Quantity |
      | B    | 88    | 0.0      |
      | C    | 77    | 0.0      |
      | A    | 99    | 0.0      |

    When I give "Ascending" sorting
    And Use it to sort
    Then The products should looks like this:
      | Name | Price | Quantity |
      | A    | 99    | 0.0      |
      | B    | 88    | 0.0      |
      | C    | 77    | 0.0      |


  Scenario: Sort by Name Descending
    Given Configuration "BaseUrl" as "http://www.example.com"
    And Configuration "ApiUrls:Products" as "/api/resource/products"
    And Mocked products to "http://www.example.com/api/resource/products":
      | Name | Price | Quantity |
      | B    | 88    | 0.0      |
      | C    | 77    | 0.0      |
      | A    | 99    | 0.0      |

    When I give "Descending" sorting
    And Use it to sort
    Then The products should looks like this:
      | Name | Price | Quantity |
      | C    | 77    | 0.0      |
      | B    | 88    | 0.0      |
      | A    | 99    | 0.0      |


  Scenario: Sort by Popularity
    Given Configuration "BaseUrl" as "http://www.example.com"
    And Configuration "ApiUrls:Products" as "/api/resource/products"
    And Configuration "ApiUrls:ShopperHistory" as "/api/resource/shopperHistory"
    And Mocked products to "http://www.example.com/api/resource/products":
      | Name | Price | Quantity |
      | B    | 88    | 0.0      |
      | C    | 77    | 0.0      |
      | A    | 99    | 0.0      |

    And Mocked history to "http://www.example.com/api/resource/shopperHistory":
      | CustomerId | ProductName | ProductPrice | ProductQuantity |
      | 001        | A           | 99           | 10              |
      | 002        | A           | 99           | 10              |
      | 003        | B           | 88           | 20              |
      | 004        | B           | 88           | 30              |
      | 005        | C           | 77           | 5               |
      | 006        | C           | 77           | 3               |



    When I give "Recommended" sorting
    And Use it to sort
    Then The products should looks like this:
      | Name | Price | Quantity |
      | B    | 88    | 0.0      |
      | A    | 99    | 0.0      |
      | C    | 77    | 0.0      |
