using System.Net.Http;
using Autofac;
using Microsoft.Extensions.Configuration;
using Moq;
using WooliesX.Products.Sorting;
using WooliesX.Service;

namespace WooliesX.Spec
{
    public abstract class StepBase
    {
        protected readonly IContainer Container;
        protected readonly Mock<IConfiguration> MockConfiguration;
        protected readonly Mock<IHttpRetryClient> MockHttpClient;

        public StepBase()
        {
            var builder = new ContainerBuilder();
            this.MockConfiguration = new Mock<IConfiguration>();
            this.MockHttpClient = new Mock<IHttpRetryClient>();

            builder.RegisterInstance(this.MockConfiguration.Object);
            builder.RegisterInstance(new Mock<IHttpClientFactory>().Object);
            builder.RegisterInstance(this.MockHttpClient.Object);
            builder.RegisterType<SortStrategyFactory>();
            builder.RegisterType<ProductService>().As<IProductService>();


            this.Container = builder.Build();
        }
    }
}