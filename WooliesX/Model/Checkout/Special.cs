using System.Collections.Generic;

namespace WooliesX.Model.Checkout
{
    public class Special
    {
        public decimal Total { get; set; }
        public IEnumerable<Quantity> Quantities { get; set; }
    }
}