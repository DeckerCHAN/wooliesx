namespace WooliesX.Model.Checkout
{
    public class Product
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}