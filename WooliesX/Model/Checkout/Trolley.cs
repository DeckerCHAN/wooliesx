using System.Collections.Generic;

namespace WooliesX.Model.Checkout
{
    public class Trolley
    {
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<Special> Specials { get; set; }
        public IEnumerable<Quantity> Quantities { get; set; }
    }
}