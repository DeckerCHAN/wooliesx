using System.Collections.Generic;
using System.Linq;

namespace WooliesX.Model.Checkout.Extensions
{
    public static class SpecialExtension
    {
        public static decimal SpecialSaving(this Special special, Dictionary<string, decimal> productPrices)
        {
            var originalPrice = special.Quantities.Where(x => productPrices.ContainsKey(x.Name))
                .Select(x => productPrices[x.Name] * (decimal) x.QuantityValue).Sum();
            var discountedPrice = special.Total;

            return originalPrice - discountedPrice;
        }
    }
}