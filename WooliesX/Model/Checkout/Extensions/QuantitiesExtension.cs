using System.Collections.Generic;
using System.Linq;

namespace WooliesX.Model.Checkout.Extensions
{
    public static class QuantitiesExtension
    {
        public static decimal ApplySpecial(this IEnumerable<Quantity> quantities, Special special)
        {
            quantities = quantities.ToArray();
            var res = 0M;
            while
            (special.Quantities.Where(x => x.QuantityValue > 0).All(x =>
                quantities.Any(y => y.Name.Equals(x.Name)) &&
                quantities.Single(y => y.Name.Equals(x.Name)).QuantityValue >= x.QuantityValue))
            {
                special.Quantities.ToList().ForEach(x =>
                {
                    var item = quantities.Single(y => y.Name.Equals(x.Name));
                    item.QuantityValue -= x.QuantityValue;
                });
                res += special.Total;
            }

            return res;
        }
    }
}