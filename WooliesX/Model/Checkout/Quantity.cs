using Newtonsoft.Json;

namespace WooliesX.Model.Checkout
{
    public class Quantity
    {
        public string Name { get; set; }

        [JsonProperty("Quantity")] public float QuantityValue { get; set; }
    }
}