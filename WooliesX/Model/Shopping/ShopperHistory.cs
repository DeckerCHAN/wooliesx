using System.Collections.Generic;

namespace WooliesX.Model.Shopping
{
    public class ShopperHistory
    {
        public int CustomerId { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}