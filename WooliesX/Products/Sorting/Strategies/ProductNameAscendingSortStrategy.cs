using System;
using WooliesX.Model.Shopping;

namespace WooliesX.Products.Sorting.Strategies
{
    public sealed class ProductNameAscendingSortStrategy : IProductSortStrategy
    {
        public int Compare(Product x, Product y)
        {
            return string.Compare(x.Name, y.Name, StringComparison.Ordinal);
        }
    }
}