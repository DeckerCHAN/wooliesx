using System;
using System.Collections.Generic;
using System.Linq;
using WooliesX.Model.Shopping;

namespace WooliesX.Products.Sorting.Strategies
{
    public sealed class ProductNameDescendingSortStrategy : IProductSortStrategy
    {
        public int Compare(Product x, Product y)
        {
            return -string.Compare(x.Name, y.Name, StringComparison.Ordinal);
        }
    }

    public sealed class ProductPopularitySortStrategy : IProductSortStrategy
    {
        private readonly Dictionary<string, float> Popularity;


        public ProductPopularitySortStrategy(IEnumerable<ShopperHistory> shopperHistories)
        {
            this.Popularity = shopperHistories.SelectMany(x => x.Products).GroupBy(x => x.Name)
                .Select(x => new Tuple<string, float>(x.Key, x.Sum(y => y.Quantity)))
                .ToDictionary(x => x.Item1, x => x.Item2);
        }

        public int Compare(Product x, Product y)
        {
            return -this.Popularity.GetValueOrDefault(x.Name, 0F)
                .CompareTo(this.Popularity.GetValueOrDefault(y.Name, 0F));
        }
    }
}