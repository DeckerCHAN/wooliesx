using WooliesX.Model.Shopping;

namespace WooliesX.Products.Sorting.Strategies
{
    public sealed class ProductLowPriceSortStrategy : IProductSortStrategy
    {
        public int Compare(Product x, Product y)
        {
            return x.Price.CompareTo(y.Price);
        }
    }
}