using WooliesX.Model.Shopping;

namespace WooliesX.Products.Sorting.Strategies
{
    public sealed class ProductHighPriceSortStrategy : IProductSortStrategy
    {
        public int Compare(Product x, Product y)
        {
            return -x.Price.CompareTo(y.Price);
        }
    }
}