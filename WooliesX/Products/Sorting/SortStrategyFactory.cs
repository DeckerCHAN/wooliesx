using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WooliesX.Model.Shopping;
using WooliesX.Products.Sorting.Strategies;
using WooliesX.Service;

namespace WooliesX.Products.Sorting
{
    public class SortStrategyFactory
    {
        private readonly IConfiguration Configuration;
        private readonly IHttpRetryClient HttpRetryClient;

        public SortStrategyFactory(IHttpRetryClient httpRetryClient, IConfiguration configuration)
        {
            this.HttpRetryClient = httpRetryClient;
            this.Configuration = configuration;
        }

        private async Task<IEnumerable<ShopperHistory>> GetShoppingHistoryFromApi()
        {
            return await this.HttpRetryClient.GetAsyncWithRetry<IList<ShopperHistory>>(
                $"{this.Configuration["BaseUrl"]}{this.Configuration["ApiUrls:ShopperHistory"]}?token={this.Configuration["TestUser:Token"]}");
        }

        public async Task<IProductSortStrategy> BuildStrategy(string strategyName)
        {
            switch (strategyName.ToUpper())
            {
                case "LOW":
                    return new ProductLowPriceSortStrategy();
                case "HIGH":
                    return new ProductHighPriceSortStrategy();
                case "ASCENDING":
                    return new ProductNameAscendingSortStrategy();
                case "DESCENDING":
                    return new ProductNameDescendingSortStrategy();
                case "RECOMMENDED":
                    return new ProductPopularitySortStrategy(await this.GetShoppingHistoryFromApi());
                default:
                    return null;
            }
        }
    }
}