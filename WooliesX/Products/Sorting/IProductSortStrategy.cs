using System.Collections.Generic;
using WooliesX.Model.Shopping;

namespace WooliesX.Products.Sorting
{
    public interface IProductSortStrategy : IComparer<Product>
    {
    }
}