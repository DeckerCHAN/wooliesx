using System.Linq;
using WooliesX.Model.Checkout;
using WooliesX.Model.Checkout.Extensions;

namespace WooliesX.Service
{
    public class TrolleyService : ITrolleyService
    {
        public decimal CalculateMinimumTotal(Trolley trolley)
        {
            var productNamePrices = trolley.Products.ToDictionary(t => t.Name, t => t.Price);
            var quantities = trolley.Quantities
                .Select(x => new Quantity {Name = x.Name, QuantityValue = x.QuantityValue}).ToArray();

            var specialPrice = trolley.Specials.OrderByDescending(x => x.SpecialSaving(productNamePrices))
                .Sum(trolleySpecial => quantities.ApplySpecial(trolleySpecial));

            var restPrice = quantities.Where(x => productNamePrices.ContainsKey(x.Name))
                .Select(x => productNamePrices[x.Name] * (decimal) x.QuantityValue).Sum();

            return specialPrice + restPrice;
        }
    }
}