using WooliesX.Model.Checkout;

namespace WooliesX.Service
{
    public interface ITrolleyService
    {
        decimal CalculateMinimumTotal(Trolley trolley);
    }
}