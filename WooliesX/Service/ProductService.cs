using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WooliesX.Model.Shopping;
using WooliesX.Products.Sorting;

namespace WooliesX.Service
{
    public sealed class ProductService : IProductService
    {
        private readonly IConfiguration Configuration;
        private readonly IHttpRetryClient HttpRetryClient;

        public ProductService(IHttpRetryClient httpRetryClient, IConfiguration configuration)
        {
            this.HttpRetryClient = httpRetryClient;
            this.Configuration = configuration;
        }

        public async Task<IList<Product>> FetchSortedProductListByStrategy(IProductSortStrategy strategy)
        {
            var products = (await this.GetRawProductsFromApi()).ToArray();

            Array.Sort(products, strategy);

            return products.ToList();
        }

        private async Task<IList<Product>> GetRawProductsFromApi()
        {
            return await this.HttpRetryClient.GetAsyncWithRetry<IList<Product>>(
                $"{this.Configuration["BaseUrl"]}{this.Configuration["ApiUrls:Products"]}?token={this.Configuration["TestUser:Token"]}");
        }
    }
}