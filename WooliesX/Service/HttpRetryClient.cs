using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace WooliesX.Service
{
    public interface IHttpRetryClient
    {
        Task<T> GetAsyncWithRetry<T>(string url);
    }

    public class HttpRetryClient : IHttpRetryClient
    {
        private readonly IHttpClientFactory ClientFactory;

        private readonly IConfiguration Configuration;


        public HttpRetryClient(IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            this.Configuration = configuration;
            this.ClientFactory = clientFactory;
        }

        public async Task<T> GetAsyncWithRetry<T>(string url)
        {
            return await (await this.SendAsyncWithRetry(new HttpRequestMessage(HttpMethod.Get, url))).Content
                .ReadAsAsync<T>();
        }

        private async Task<HttpResponseMessage> SendAsyncWithRetry(HttpRequestMessage message)
        {
            for (var i = 0; i < this.Configuration.GetValue<int>("HttpMaxRetry"); i++)
                using (var client = this.ClientFactory.CreateClient())
                {
                    var resp = await client.SendAsync(message);
                    if (resp.IsSuccessStatusCode) return resp;
                }

            throw new Exception($"Unable to send to {message.RequestUri} after max retries.");
        }
    }
}