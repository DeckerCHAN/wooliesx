using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesX.Model.Shopping;
using WooliesX.Products.Sorting;

namespace WooliesX.Service
{
    public interface IProductService
    {
        Task<IList<Product>> FetchSortedProductListByStrategy(IProductSortStrategy strategy);
    }
}